package nix.autoframework.home.spec

import geb.spock.GebReportingSpec
import nix.autoframework.home.page.StartPage
import nix.autoframework.home.page.BlogPage

class NixNavigationSpec extends GebReportingSpec{

    def "Navigate to Start page"(){
        when:
            to StartPage

        and:
            "User navigates to Blog page"()

        then:
            at BlogPage
    }
}
